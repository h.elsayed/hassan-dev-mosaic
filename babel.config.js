module.exports = {
  presets: [
    '@vue/app',
    '@babel/preset-typescript',
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current'
        }
      }
    ]
  ],
  plugins: [
    'transform-remove-console',
    '@babel/plugin-syntax-dynamic-import'
  ],
  include: [
    './node_modules/@rentsync/client-website-core',
    './node_modules/@rentsync/client-website-components'
  ]
};
