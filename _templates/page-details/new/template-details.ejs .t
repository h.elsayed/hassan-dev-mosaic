---
to: "./src/pages/<%= h.changeCase.pascal(name) %>/<%= h.changeCase.pascal(name) %>Details/<%= h.changeCase.pascal(name) %>Details.vue"
unless_exists: true
---
<template>
  <div class="<%= h.changeCase.kebab(name) %>-details">
    <h1>{{ this.pageData.title }}</h1>
  </div>
</template>

<script lang="ts">
import Vue from 'vue'
import { Page } from '@rentsync/client-website-core/dist/models/Page'

export default Vue.extend({
  props: {
    pageData: Page,
  },
  data() {
    return {}
  },
  async created() {},
})
</script>

<style lang="scss"></style>
