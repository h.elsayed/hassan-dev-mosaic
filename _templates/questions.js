const validate = require('./validations')

module.exports = questions => ({ prompter, args }) => {
  const providedArgs = questions.reduce((selectedArgs, question) => {
    if (args[question.name]) selectedArgs[question.name] = args[question.name];
    return selectedArgs;
  }, {});

  return prompter
    .prompt(questions.filter(({ name }) => !providedArgs[name]))
    .then(answers => {
      const validatedAnswers = validate(questions, answers)

      if (validatedAnswers.answersAreValid) {
        return { ...answers, ...providedArgs }
      } else {
        for (const error of validatedAnswers.errors) {
          console.log(`\n ${error}`)
        }
      }
    });
};