module.exports = (questions, answers) => {
  let answersAreValid = true
  const errors = []

  for (const question of questions) {
    if (question.rules.length >= 1) {
      const promptName = answers[question.name]

      for (const rule of question.rules) {
        switch (rule) {
          case 'not-empty':
            if (promptName === '' || typeof promptName !== 'string') {
              answersAreValid = false
              errors.push(`${question.name} cannot be left blank + must be of type string.`)
            }
            break;

          case 'no-spaces':
            if (promptName.indexOf(' ') > 0 || typeof promptName !== 'string') {
              answersAreValid = false
              errors.push(`${question.name} cannot contain spaces.`)
            }
            break;

          default:
            break;
        }
      }
    }
  }

  return {
    answersAreValid,
    errors
  }
}