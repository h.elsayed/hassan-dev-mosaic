let allAnswers = {}

module.exports = {
  prompt: ({ prompter, args }) => {
    return prompter.prompt({
      type: "input",
      name: "name",
      message: "Name of Page",
      validate: (value) => {
        if(value === ''){
          return 'Page must have a name'
        }
        return true
      },
      result: (value) => {
        allAnswers['name'] = value
        return value
      }
    }).then(() => {
      return ({...allAnswers, ...args})
    })
  }
}