---
to: "./src/store/modules/<%= h.changeCase.kebab(name) %>.ts"
unless_exists: true
---
<%
  const folderName = h.changeCase.kebab(name)
  const fileName = h.changeCase.pascal(name)
  const lowerCased = h.changeCase.lower(name)
  const importName = h.changeCase.pascal(name)
  const upperCased = h.changeCase.constant(name)
%>import { ActionContext } from 'vuex'
import { RootState } from '@rentsync/client-website-core/dist/types/store'

//  state
const state = {

}

// getters
const getters = {
}

// mutations
const mutations = {
  SET_<%= upperCased %>: (moduleState: any, payload: any) => {
    moduleState = payload
  }
}

// actions
const actions = {
  set<%= fileName %>: ({commit}: ActionContext<any, RootState>, payload: any) => {
    commit('SET_<%= upperCased %>', payload)
  }
}

// export our '<%= lowerCased %>' store
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
