/**
 * This file is used if you want to include a vue component globablly and not have to import in every component.
 * Globally regersitering components should be a last resort and we prefer to see you import the components as needed. But there are always edge cases.
 *
 * You can add the component here by first importing it:
 * import CustomHeaderBtn from './components/custom/CustomHeaderBtn/CustomHeaderBtn.vue'
 *
 * Then you can include it like so:
 * Vue.component('custom-header-btn', CustomHeaderBtn)
 *
 * You are now free to use the custom element in any .vue file: <custom-header-btn></custom-header-btn>
*/

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Vue from 'vue'
