import Vue from 'vue'
import Vuex from 'vuex'
import { Core } from '@rentsync/client-website-core'
import storeModules from './modules/storeModules'

Vue.use(Vuex)
interface Modules {
  [key: string]: any
}

// Merge website store modules with Core modules
const modules: Modules = Core.helpers.generic.combineObjects(
  Core.storeModules,
  storeModules,
)

const store = new Vuex.Store({
  modules,
})

const createVuexStore = async (VuexStore: any) => {
  VuexStore.dispatch('client/setClient')

  return VuexStore
}

export default createVuexStore(store)
