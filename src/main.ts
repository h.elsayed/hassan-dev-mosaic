// Rentsync
import { Core } from '@rentsync/client-website-core'
import './environment-config'
import './custom-components'
import '@/styles/main.scss'

// Vue Packages
import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import VuexStore from './store'

// eslint-disable-next-line
import '@rentsync/client-website-core/dist/lib/vendors/uikit-3/js/uikit.min.js'

Vue.use(Router)
Vue.use(Core)

Vue.config.productionTip = false

VuexStore.then((store: any) => {
  Core.router
    .init(store.getters['client/getClientBasic'])
    .then((router: any) => {
      Vue.prototype.$client = store.getters['client/getClient']

      new Vue({
        router,
        store,
        render: (h) => h(App),
      }).$mount('#app')
    })
})
