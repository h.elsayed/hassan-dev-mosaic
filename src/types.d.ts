import Vue from 'vue'
import {
  PropertyApiFunctions,
  WebsiteApiFunctions,
  ClientApiFunctions,
  SiteSettingsApiFunctions,
} from '@rentsync/client-website-core/dist/types/api/api-functions'
import { Client } from '@rentsync/client-website-core/dist/models/Client'
import { FrontendHelpers } from '@rentsync/client-website-core/dist/types/lib/helpers/generic/helper-functions'
import { DateTimeFunctions } from '@rentsync/client-website-core/dist/types/lib/helpers/date-time/functions'
import { TextFunctions } from '@rentsync/client-website-core/dist/types/lib/helpers/text/text-functions'
import TranslationsHelpers from '@rentsync/client-website-core/dist/lib/helpers/translations'

declare module 'vue/types/vue' {
  interface Vue {
    $rs: {
      settings: SiteSettingsApiFunctions;
      property: PropertyApiFunctions;
      website: WebsiteApiFunctions;
      client: ClientApiFunctions;
    };
    $client: Client;
    $genericHelpers: FrontendHelpers,
    $dateTimeHelpers: DateTimeFunctions,
    $textHelpers: TextFunctions,
    $translationHelpers: typeof TranslationsHelpers
  }
}
