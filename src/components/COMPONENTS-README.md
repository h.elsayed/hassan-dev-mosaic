This components folder located on the website level is only to be used for reusable code you have on this specific website.

All other components should be built and pulled in from the component library. 99.9% of the time, the component you are building can be used by other clients. This components folder is great though if you have a bunch of global repeated code on this client's website. You can place your components here in that case.
