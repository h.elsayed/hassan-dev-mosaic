See the hygen template scripts on how to generate new pages. You should never be creating a page manually.

If there is something missing from the hygen templates, please feel free to make a merge request to the Website Base Template repo and have a Maintainer take a look at it.

Some useful scripts for generating page folders:

`npm run new-page` - This will generate you a page folder, a .vue file and a components folder.

`npm run new-page-details` - This will generate you a page folder, a .vue file and a components folder with a Details subfolder for that page.
