import { Core } from '@rentsync/client-website-core'

// Client Key
const clientKey = process.env.VUE_APP_CLIENT_KEY
  ? process.env.VUE_APP_CLIENT_KEY
  : ''

// Database[Environment]
const environment = process.env.VUE_APP_DB_ENV
  ? process.env.VUE_APP_DB_ENV
  : 'development'

// Api Version
const apiVersion = process.env.VUE_APP_API_VERSION
  ? process.env.VUE_APP_API_VERSION
  : 'v1'

// AssetHost Version
const assetHost = process.env.VUE_APP_ASSET_HOST
  ? process.env.VUE_APP_ASSET_HOST
  : 'aws'

// ** Set Website Configuration **
Core.setConfig({
  clientKey,
  env: environment,
  version: apiVersion,
  assetHost
})
