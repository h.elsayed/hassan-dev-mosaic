'use strict';
// HtmlWebpack Plugin provided by Vue is only 3.2, this will break at 4.0+.
// https://www.npmjs.com/package/html-webpack-plugin to set up correct hooks when time comes
const cheerio = require('cheerio');

class SSRPlugin {
  constructor() {

  }
  apply(compiler) {
    if(process.env.CI) {
      compiler.plugin('compilation', (compilation) => {
        compilation.plugin('html-webpack-plugin-alter-asset-tags',
            (data) => {
              if(data.outputName === 'index.html') {
                if(data.head.length) {
                  for(let i =0; i < data.head.length; i++) {
                    data.head[i].attributes['data-dp'] = 'true'
                    if(data.head[i].tagName === 'script') {
                      data.head[i].attributes['defer'] = 'defer'
                    }
                  }
                }

                if(data.body.length) {
                  for(let i =0; i < data.body.length; i++) {
                    data.body[i].attributes['data-dp'] = 'true'
                    if(data.body[i].tagName === 'script') {
                      data.body[i].attributes['defer'] = 'defer'
                    }
                  }
                }
              }
            }
          )
        compilation.plugin('html-webpack-plugin-after-html-processing',
          (data) => {
            if(data.html) {
              const $ = cheerio.load(data.html, {
                decodeEntities: false
              })
              const dataAttr = $('[data-dp]').not('[data-dp="true"]')
              if(dataAttr.length) {
                throw '"data-dp" is a restricted data attribute, please use something else'
              }
              $('head').children().attr('data-dp','true')
              $('body').children().attr('data-dp','true')
              $('div#app').css('visibility','hidden')
              let html = $.html()
              data.html = html

            }
          })
        }
      )
    }
  }
}


module.exports = SSRPlugin;