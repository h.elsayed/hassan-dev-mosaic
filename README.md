# Website Base Template

## Setup Gitlab Repository

1. `cd` into your `client-websites` directory and run the following command, **replacing `client_key` with the actual client's key**
   ```
   git clone git@gitlab.com:llws/platform/client-websites/website-base-template.git client_key
   ```
2. `cd` into that newly created project folder
   ```
   cd path-to-working-directory/client-websites/client_key
   ```
3. Run the following command to remove the initial git origin
   ```
   git remote remove origin
   ```
4. Set new origin
   ```
   git remote add origin https://gitlab.com/llws/platform/client-websites/client_key
   ```
5. Open `package.json` within the project and update the project name and version. Add and commit these changes.
   ```
   "name": "client_key",
   "version": "0.1.0",
   ```
6. Do initial push to repository
   ```
   git push origin master
   ```
7. Confirm that project was successfully pushed to Gitlab.

## Website Configuration

### Environment

Copy the `.env.example` file and create a new file called `.env`

1. Set `VUE_APP_CLIENT_KEY` to the siteKey of the website you're working on<br>

   ```
   VUE_APP_CLIENT_KEY=lws_site
   ```

2. Set `VUE_APP_DB_ENV` to the environment of the website you're working on<br>
   ```
   VUE_APP_DB_ENV=production
   ```
3. `VUE_APP_API_VERSION` will always be set to `v1` unless we ever work on a completely new API.
   **Do not change this value.**

4. `VUE_APP_ENV` Will set how the app should run (production, staging, development). Only used by some components such as Cookie Policy to render scripts only on staging ENV.
   ```
   VUE_APP_ENV=staging
   ```

### Installing Dependancies

`npm install`

`npm rebuild node-sass` - needed to rebuild a binary for your linux env

## Getting Started
### Running Project In Development

`npm run serve` - Will compile and hot-reload for development

### Styling
`src/styles/_config.scss` is where you'll set your colors, fonts and any other required global variables. **All Google fonts will need to be embedded via @import option**<br>

`_general.scss` is where you'll set all all general styles. This includes general classes/styles, setting the font-family for all HTML tags, etc.

`main.scss` is where you'd import any `scss` stylesheet to be compiled.

All component specific styles should be made within the `<style lang="scss"></style>` tags within each specific component vue file.

#### Adding relative paths in scss files

When you need to include a local asset you need to use special syntax. In the below example we will set an svg located in the project as a background image:

```scss
background-image: url(~@/assets/images/svg/arrow-right.svg');
```

The `~@` syntax will point you to the `src` folder no matter where you are calling this from. This is important expecially in files found under the `styles` folder because they are imported into every single component and file, thus making it impossible for us to know the relative path.

## Access Client Data
In your Vue Templates, you can use `this.$client` to access client data.

## Creating a new page

`npm run new-page <PageName>` - where <pageName> is what you want to call the page. Will convert to PascalCase. This genrates the `.vue`, `.scss` and `.ts` files

#### (Optional) How to create a new global component

##### Step 1

`npm run new-gc <Name>` - Works just like pages, but places the folder under `./src/components/custom`

##### Step 2

`npm run register-gc` - This command will register your new component globally by writing it to the `./src/customComponents.ts` file.

### Run your tests

`npm run test`

### Compiles and minifies for production

`npm run build`

### Lints and fixes Typescript files

`npm run lint`
