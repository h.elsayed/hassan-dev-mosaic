const StyleLintPlugin = require('stylelint-webpack-plugin')
const StyleLintConfig =
  './node_modules/@rentsync/client-website-core/dist/.stylelintrc.json'
const webpack = require('webpack')
const fs = require('fs')
const SSRPlugin = require('./plugins/ssr-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');

var isWin = process.platform === "win32";

if(isWin) {
  const filePath = process.cwd()
  let i = 1
  const uppercaseReg = new RegExp('[A-Z]')
  while (i <= filePath.length){
    character = filePath.charAt(i);
    if (uppercaseReg.test(character)){
      throw new Error("Congrats You Played Yourself! Change your filepathing to have No Caps in it!\n\n")
    }
    i++;
  }
}
// HACK currently this does not look or make sub folders
// THIS MAY BREAK YOUR BUILD IF YOU CREATE A SUBFOLDER in src/static-pages
const staticPageLoader = (folder, plugins) => {
  const files = fs.readdirSync(folder);
  if (folder[folder.length - 1] !== '/') {
    folder += '/'
  }
  return files
    .map(file => {

      return new HtmlWebpackPlugin({
        template: folder + file,
        filename: 'pages/' + file,
        inject: false,
      });
    })
    .concat(plugins)
};

const vueConfig = {
  css: {
    loaderOptions: {
      scss: {
        data: `@import '@/styles/globals.scss';`
      },
    },
    extract: {
      //For SSR, please do not change
      chunkFilename: 'css/dp.[id].[chunkhash].css'
    }
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('@assets', path.resolve('src/assets'))
    config.resolve.alias.set('@images', path.resolve('src/assets/images'))
    config.resolve.alias.set('@pages', path.resolve('src/pages'))
    config.resolve.alias.set('@components', path.resolve('src/components'))
    config.resolve.alias.set('@error', path.resolve('src/pages/Error/Error.vue'))
    config.resolve.alias.set('@layouts', path.resolve('src/layouts'))


    // Key Components
    config.resolve.alias.set('@footer', path.resolve('src/layouts/Footer'))
    config.resolve.alias.set('@header', path.resolve('src/layouts/Header'))

    // Component Library Paths
    config.resolve.alias.set(
      '@molecules',
      path.resolve(
        'node_modules/@rentsync/client-website-components/dist/components/molecules'
      )
    )
    config.resolve.alias.set(
      '@atoms',
      path.resolve(
        'node_modules/@rentsync/client-website-components/dist/components/atoms'
      )
    )
    config.resolve.alias.set(
      '@organisms',
      path.resolve(
        'node_modules/@rentsync/client-website-components/dist/components/organisms'
      )
    )
    config.resolve.alias.set(
      '@templates',
      path.resolve(
        'node_modules/@rentsync/client-website-components/dist/components/templates'
      )
    )
    config.resolve.alias.set(
      '@lib',
      path.resolve(
        'node_modules/@rentsync/client-website-components/dist/lib'
      )
    )
    config.resolve.alias.set(
      'vue$',
      'vue/dist/vue.runtime.esm.js'
    )
    // ignore our set up scripts in build
    config.plugin('copy').tap((args) => [
      [
        {
          from: path.resolve('./public'),
          to: path.resolve('./dist'),
          toType: 'dir',
          ignore: ['addProps.js', 'importStyles.js'],
        },
      ],
    ])

    // disable prefetch/preload, we want true lazy load
    config.plugins.delete('prefetch')
    config.plugins.delete('preload')

    //Custom plugin to add SSR attriibutes
    config.plugin('SSR').use(SSRPlugin)
  },
  configureWebpack: {
    output: {
      //For SSR, please do not change
      chunkFilename: "js/dp.[id].[chunkhash].js"
    },
    plugins: staticPageLoader('src/static-pages', [
      new StyleLintPlugin({
        configFile: StyleLintConfig,
        fix: true,
      }),
      new webpack.ProvidePlugin({
        UIkit3: path.resolve(path.join(__dirname, './node_modules/@rentsync/client-website-core/dist/lib/vendors/uikit-3/js/uikit.min.js'))
      })
    ]),
    optimization: {
      usedExports: true,
      sideEffects: true,
      minimize: true,
      minimizer: [
       new TerserPlugin({
        cache: true,
        extractComments:true,
        parallel: true,
        terserOptions: {
          mangle: true
        }
       })
      ],
      runtimeChunk: true
    },
    devServer: {
      open: true
    },
  },
}

if(process.env.VUE_APP_CDN_ENVIRONMENT) {
  const pathToAssets = `https://cdn.rentsync.com/${process.env.VUE_APP_CDN_ENVIRONMENT}/${process.env.VUE_APP_CLIENT_KEY}/`
  vueConfig.publicPath = pathToAssets
}

module.exports = vueConfig;
