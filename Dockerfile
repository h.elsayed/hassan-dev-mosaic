From node:12.16.2

WORKDIR /app

RUN npm i -g http-server

ADD . /app

RUN npm run build

CMD http-server /app/dist -p 5000