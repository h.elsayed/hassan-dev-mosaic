# New Features

( Add description of what this feature is. Detailed as possible )

# Version

( Please indicate if this is a major, minor or patch. Major = breaking changes, Minor = changes that do not break anything, Patch = fixes that can be applied to older version.)

# What was added in this Feature

( Bullet points that can include functions, styles, types, tests, components, etc )
