let express = require('express');
let app = express();
let path = require('path');

app.use(express.static(path.resolve(__dirname, '../dist')));

app.get('/', function (req, res) {
    res.render('index.html', {});
  });

app.listen(8080);
console.log('listening on port 8080')